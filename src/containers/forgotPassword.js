import React from 'react'
import { Text, View,Platform,TouchableOpacity, TVEventHandler, useTVEventHandler } from 'react-native'
import BackgroundTheme from '../components/backgroundTheme'
import RoundButton from '../components/roundButton';
import Textinput from '../components/textInput';
export default function ForgotPassword(){
    const [lastEventType, setLastEventType] = React.useState('');

    const myTVEventHandler = evt => {
      setLastEventType(evt.eventType);
    };
  
    useTVEventHandler(myTVEventHandler);
console.warn(lastEventType)
        return (
           <BackgroundTheme>
                <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <View>
          <Text style={{color: 'white', fontSize: 30, fontWeight: 'bold'}}>
            Forgot Password
          </Text>
        </View>
        <View style={{padding: Platform.isTV ? '2%' : '5%'}}>
          <Text style={{color: 'white', fontSize: 20}}>
            Please provide your registered email address below
          </Text>
        </View>
        <Textinput placeholder="Enter your email" />
        <RoundButton placeholder="Next"/>
       
      </View>
           </BackgroundTheme>
        )
    
}
