import React from 'react';
import {View, Text, Platform, TouchableOpacity , TVEventHandler, useTVEventHandler} from 'react-native';
import BackgroundTheme from '../components/backgroundTheme';
import RoundButton from '../components/roundButton';
import Textinput from '../components/textInput';

export default function Login(props) {

    const [lastEventType, setLastEventType] = React.useState('');

    const myTVEventHandler = evt => {
      setLastEventType(evt.eventType);
    };
  
    useTVEventHandler(myTVEventHandler);
console.warn(lastEventType)
  return (
    <BackgroundTheme>
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <View>
          <Text style={{color: 'white', fontSize: 30, fontWeight: 'bold'}}>
            Welcome!
          </Text>
        </View>
        <View style={{padding: Platform.isTV ? '2%' : '5%'}}>
          <Text style={{color: 'white', fontSize: 20}}>
            Please Login to continue
          </Text>
        </View>
        <Textinput placeholder="User Name" />
        <Textinput placeholder="Password" />
        <View
          style={{
            paddingTop: Platform.isTV ? '2%' : '5%',
            alignSelf: 'flex-end',
            width: Platform.isTV ?'37%':'40%'
          }}>
          <TouchableOpacity onPress={()=>{
              props.navigation.navigate("ForgotPassword")
          }}>
            <Text style={{color: 'white', fontSize: 15}}>Forgot Password?</Text>
          </TouchableOpacity>
        </View>
        <RoundButton placeholder="Continue"/>
        <View style={{paddingTop:"4%"}}>
            <TouchableOpacity>
                <Text style={{color: 'white', fontSize:15}}>
                    Don't have an account? Sign up
                </Text>
            </TouchableOpacity>
        </View>
      </View>
    </BackgroundTheme>
  );
}
