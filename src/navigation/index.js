import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../containers/login';
import ForgotPassword from '../containers/forgotPassword';


const Stack = createStackNavigator();

function Routes(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown:false}}>
        <Stack.Screen name="Login" component={Login} {...props} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} {...props}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;