import React from 'react';
import {Text, View, TouchableOpacity, Platform} from 'react-native';

export default function RoundButton(props) {
  return (
    <View
      style={{
        width: Platform.isTV ? '50%' : '80%',
        paddingVertical: Platform.isTV ? '1%' : '4%',
        marginTop: Platform.isTV ? '2%' : '10%',
        borderWidth: 1,
        borderColor: 'red',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: 'red',
        // height:"7%",
        // justifyContent:"center",
      }}>
      <TouchableOpacity>
        <Text style={{color: 'white', fontSize:20}}>{props.placeholder}</Text>
      </TouchableOpacity>
    </View>
  );
}
