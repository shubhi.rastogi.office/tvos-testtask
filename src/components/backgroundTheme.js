import React, { Component } from 'react'
import {  View } from 'react-native'

export default class BackgroundTheme extends Component {
    render() {
        return (
            <View style={{flex: 1, backgroundColor: "#171414"}}>
                {this.props.children}
            </View>
        )
    }
}
