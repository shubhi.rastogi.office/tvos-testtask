import React, {useState} from 'react';
import {Text, View, TextInput, Platform} from 'react-native';

export default function Textinput(props) {
  let [value, setValue] = useState('');
  return (
    <View
      style={{
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        width: Platform.isTV ? '50%' : '80%',
        paddingVertical:Platform.isTV? 0: "4%"
      }}>
      <TextInput
        value={value}
        placeholder={props.placeholder}
        placeholderTextColor="white"
      />
    </View>
  );
}
