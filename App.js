/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import ReactNative, {
  Platform,
  View,
} from 'react-native';
// 
import Routes from './src/navigation';
const StatusBar = Platform.isTV ? View : ReactNative.StatusBar;

const App: () => React$Node = (props) => {
  console.log("Please")
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={{flex: 1}}>
      <Routes {...props}/>
      </View>
      
    </>
  );
};


export default App;
